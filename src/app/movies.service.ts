import {Inject, Injectable} from '@angular/core';
import {SearchInput} from './search/search-input';
import {Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  searchInput: SearchInput = new SearchInput();

  private url = '';
  public results;
  public movieState: Subject<any> = new Subject();
  public movie$ = this.movieState.asObservable();

  constructor(private http: HttpClient, @Inject(LOCAL_STORAGE) private storage: WebStorageService) {
  }

  loadMovies(searchInput) {
    this.url = `https://api.themoviedb.org/3/search/movie?api_key=4cb1eeab94f45affe2536f2c684a5c9e&query=${searchInput}`;
    return this.http.get(`${this.url}`);
  }

  public setResults(results) {
    this.results = results;
    this.movieState.next({
      data: results
    });
  }

  getDetails(id) {
    return this.http.get(`https://api.themoviedb.org/3/movie/${id}?api_key=4cb1eeab94f45affe2536f2c684a5c9e`);
  }

  isFavMovie(id) {
    const favs = this.storage.get('favorites') || [];
    if (favs.find(x => x.id === id)) {
      return 'heart-red';
    } else {
      return '';
    }
  }

  toggleFav(fav) {
    let favs = this.storage.get('favorites') || [];
    if (favs.find(x => x.id === fav.id)) {
      favs = favs.filter(obj => obj.id !== fav.id);
    } else {
      favs.push(fav);
    }
    this.storage.set('favorites', favs);
  }

  getFavs() {
    return this.storage.get('favorites');
  }
}
