import {Component, Inject, Input, OnInit, ViewContainerRef} from '@angular/core';
import {MoviesService} from '../movies.service';
import {SearchInput} from '../search/search-input';
import {LOCAL_STORAGE, WebStorageService} from 'angular-webstorage-service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  constructor(
    private moviesService: MoviesService,
    @Inject(LOCAL_STORAGE) private storage: WebStorageService
  ) {}

  @Input() favourites;
  searchInput: SearchInput = new SearchInput();
  movies: any;

  private moviesData: any;

  ngOnInit() {
    if (this.favourites) {
      this.movies = this.moviesService.getFavs();
      return;
    }
    this.moviesData = this.moviesService.movie$.subscribe(results => {
      this.movies = results.data;
    });
  }

  public isFav(id): string {
    return this.moviesService.isFavMovie(id);
  }

  toggleFav(event, fav) {
    event.stopPropagation();
    this.moviesService.toggleFav(fav);
  }

}
