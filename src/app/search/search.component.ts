import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../movies.service';
import {SearchInput} from './search-input';
import {take} from 'rxjs/operators';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  /**
   * Search input text by user
   */
  searchInput: SearchInput = new SearchInput();

  constructor(private moviesService: MoviesService) { }

  ngOnInit() {
  }

  onSearchChange() {
    return this.moviesService.loadMovies(this.searchInput.searchInput).pipe(take(1)).subscribe((res: any) => {

      this.moviesService.setResults(res.results);
      this.searchInput.searchInput = '';
    });

  }

}
